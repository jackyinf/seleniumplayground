﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace SeleniumPlayground
{
    [TestFixture]
    public class Tests
    {
        private IWebDriver driver;

        [TestFixtureSetUp]
        public void SetupTest()
        {
            driver = new FirefoxDriver();
        }

        [TestFixtureTearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
        }

        [Test]
        public void Simple()
        {
            Assert.IsTrue(true);
        }

        [Test]
        public void SimpleSeleniumTest()
        {
            driver.Navigate().GoToUrl("http://www.seleniumhq.org/");
            Thread.Sleep(100);
            driver.FindElement(By.LinkText("Projects")).Click();
            Thread.Sleep(100);
            driver.FindElement(By.LinkText("Download")).Click();
        }

        [Test]
        public void SimpleEtisTest()
        {
            driver.Navigate().GoToUrl("http://localhost:7055/");
            driver.FindElement(By.Id("btnMainLogin")).ClickAndWait(10000);
            driver.FindElement(By.LinkText("ETISe parooliga")).ClickAndWait(300);
            driver.FindElement(By.Id("Email")).Clear();
            driver.FindElement(By.Id("Email")).SendKeys("admin200");
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys("admin200");
            driver.FindElement(By.Name("Login")).ClickAndWait(3000);
            driver.FindElement(By.XPath("//div[@id='navBarTop']/ul/li[10]/a/i")).Click();
        }
    }

    public static class Helper
    {
        public static void ClickAndWait(this IWebElement element, int timeout)
        {
            element.Click();
            Thread.Sleep(timeout);
        }
    }
}

